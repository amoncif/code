<?php

namespace App\Console\Commands;

use App\Console\Commands\Lib\ImportDataProvider;
use App\Console\Commands\Lib\ObjectsBuilder;
use Bayard\Admin\Contentmanager\Models\ContentclassModel;
use Bayard\Admin\Contentmanager\Models\ContentobjectModel;
use Bayard\Admin\Tagmanager\Models\TagModel;
use Illuminate\Console\Command;


class ImportEnfant extends Command
{
    protected $signature = 'importenfant:data {--type=} {--force=}';
    protected $description = 'Import des contenus Enfant.com';

    private $dataProvider;
    private $objectsBuilder;

    public function __construct(ImportDataProvider $dataProvider, ObjectsBuilder $objectsBuilder)
    {
        parent::__construct();
        $this->dataProvider = $dataProvider;
        $this->objectsBuilder = $objectsBuilder;
    }

    const FRONT_PAGE_ID = 1;

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->runImport();
    }

    private function runImport()
    {
        $options = $this->options();
        $type = (isset($options['type'])) ? $options['type'] : null;

        \Config::set('queue.active', false);
        $this->importData($type);
        \Config::set('queue.active', true);

    }

    private function importData($type): void
    {
        $postType = [
            'article' => [1],
            'prenom' => [8],
            'diaporama' => [4, 5],
            'recette' => [7],
        ];

        if ($type == 'menu' || $type == null) {
            $this->importMenu();
        } else {
            $this->info("\n === " . ucfirst($type) . " === ");
            if ($type == 'frontpage') {
                $this->importPostCategory();
            } else if ($type == 'tag') {
                $this->importTags();
            } else {
                $this->importPosts("" . ucfirst($type) . "", $postType[$type] ?? [], 'post_', $type);
            }
        }
    }

    private function importFrontPage($postCategories, $parentId, $idObject) //cette fonction à tester
    {
        foreach ($postCategories as $postCategorie) {
            $categorie = ContentobjectModel::where('remote_id', 'postcategory_' . $postCategorie->postcategoryid)
                ->where('contentclass_id', self::FRONT_PAGE_ID)
                ->first();

            if (empty($categorie)) {
                $parentId = $parentId ? $idObject : self::FRONT_PAGE_ID;
                $data = $this->objectsBuilder->generateDataForContentClass(self::FRONT_PAGE_ID,
                    $postCategorie->postcategoryid, $parentId, $postCategorie->title, $postCategorie->name, $postCategorie->mdescription, ContentobjectModel::STATUS_EDITED
                ) ;

                $contentObject = $this->objectsBuilder->buildContectObject($data);
                $contentObject->save();

                $this->objectsBuilder->buildBlock($contentObject);
                $contentObjectUrl = $contentObject->url;
                $contentObjectId = $contentObject->id;
                unset($contentObject);

                if (!is_null($postCategorie->url)) {
                    $this->objectsBuilder->buildRedirection($postCategorie->url, $contentObjectUrl);
                }

                $contentClass = ContentclassModel::where('identifier', 'frontpage')->first();
                $this->objectsBuilder->buildFrontPageAttribute($contentClass, $contentObjectId);
                unset($contentClass);

                echo "+";

                $this->importPostCategory($postCategorie->postcategoryid, $contentObjectId);
            } else {
                echo ".";
            }

            unset($categorie);
        }
    }

    private function importContentObject($posts, $suffixRemoteId, $contentClassIdentifier) //cette fonction à tester
    {
        foreach ($posts as $post) {
            $cmsPostObject = ContentobjectModel::where('remote_id', $suffixRemoteId . $post->postid)->first();
            if (empty($cmsPostObject)) {
                unset($cmsPostObject);
                echo "!";
                $cmsPostParentObject = ContentobjectModel::where('remote_id', 'postcategory_' . $post->postcategoryid)->first();

                if ($cmsPostParentObject) {
                    $contentClass = ContentclassModel::where('identifier', $contentClassIdentifier)->first();
                    $data = $this->objectsBuilder->generateDataForContentObject(
                        $post->postid, $contentClass->id, $cmsPostParentObject->id, $post->title, $post->resume, ContentobjectModel::STATUS_EDITED
                    );

                    $contentObject = $this->objectsBuilder->buildContectObject($data);
                    $contentObject->save();

                    if ($post->url != null) {
                        $this->objectsBuilder->buildRedirection($post->url, $contentObject->url);
                    }
                    if($contentClass) {
                        $this->objectsBuilder->buildContentObjectAttributes($post, $contentClass, $contentObject);
                    }
                }

            } else {
                echo ".";
            }
        }
    }

    private function importPostCategory(int $parentId = null, int $idObject = null)
    {
        $postCategories = $this->dataProvider->getPostCategories($parentId);

        $this->importFrontPage($postCategories, $parentId, $idObject);

        unset($postCategories);

    }

    private function importPosts($titleForDisplay, $postTypeIds, $suffixRemoteId, $contentClassIdentifier, $cpt = 0, $limit = 200)
    {
        $postsCount = $this->dataProvider->getPostsCount($postTypeIds);

        while ($cpt < $postsCount) {
            $posts = $this->dataProvider->getPostsToImport($postTypeIds, $cpt, $limit);
            $this->info("\n$titleForDisplay :" . $cpt . "/" . $postsCount);
            $this->importContentObject($posts, $suffixRemoteId, $contentClassIdentifier);
            sleep(1);
            $cpt += $limit;
            unset($posts);
        }
    }

    private function importMenu()
    {
        //importer le sur header menu
        foreach (ImportDataProvider::MENU_SUR_HEADER as $link => $nameItem) {
            $menuItem = $this->objectsBuilder->builSurHeaderMenu($nameItem);
            $menuItem->save();
        }

        //importer le menu database
        $this->objectsBuilder->buildMenu($this->dataProvider->getMenuParentsAndChilds());

    }

    private function importTags()
    {
        $data = [];
        foreach ($this->dataProvider->getTagsToImport() as $tag) {
            if (empty(TagModel::where('name', $tag->name)->first())) {
                echo "!";
                $data[$tag->posttagid] = [
                    'id' => $tag->posttagid,
                    'name' => $tag->name
                ];
            } else {
                echo ".";
            }
        }

        $this->objectsBuilder->buildTag($data);

        echo "attacher les tag aux content objects ! ";
        $this->objectsBuilder->attachTagToContentObject($this->dataProvider->getPostTagIds());

        unset($data);
    }


}
