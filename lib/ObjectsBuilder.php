<?php

namespace App\Console\Commands\Lib;

use Bayard\Admin\Contentmanager\Blocks\Xitems;
use Bayard\Admin\Contentmanager\Models\BlocksModel;
use Bayard\Admin\Contentmanager\Models\ContentclassModel;
use Bayard\Admin\Contentmanager\Models\ContentobjectAttributeModel;
use Bayard\Admin\Contentmanager\Models\ContentobjectModel;
use Bayard\Admin\Menumanager\Models\MenuItemModel;
use Bayard\Admin\Redirectionmanager\Models\RedirectionModel;
use Bayard\Admin\Tagmanager\Models\TagContentObjectModel;
use Bayard\Admin\Tagmanager\Models\TagModel;

class ObjectsBuilder
{
    private $dataProvider;

    const RECETTE = 7;

    public function __construct(ImportDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    public function buildContectObject($data)
    {
        return contentObjectModel::create([
            "contentclass_id" => $data["contentclass_id"],
            "parent_id" => $data["parent_id"],
            "name" => $data["name"],
            "metadatas" => [
                "urltitle" => $data["name"],
                "metatitle" => $data["metatitle"],
                "metadesc" => $data["metadesc"],
                "metarobots" => $data["metarobots"],
            ],
            "tmpinfos" => $data["tmpinfos"],
            "status" => ContentobjectModel::STATUS_EDITED,
            "remote_id" => $data['remote_id']
        ]);
    }

    public function buildFrontPageAttribute($contentClass, $contentObjectId)
    {
        $frontpageAttributes = [];
        foreach ($contentClass->contentclassattributes as $contentclassattribute) {
            $value = null;
            $frontpageAttributes[] = ['contentclassattribute_id' => $contentclassattribute->id,
                'contentobject_id' => $contentObjectId,
                'jsondatas' => null];
        }

        foreach ($frontpageAttributes as $frontpageAttribute) {
            ContentobjectAttributeModel::create($frontpageAttribute);
        }
        unset($frontpageAttributes);
    }

    public function buildContentObjectAttributes($post, $contentClass, $contentObject)
    {
        $articleAttributes = [];
        foreach ($contentClass->contentclassattributes as $contentclassattribute) {
                $articleAttributes[] = [
                    'contentclassattribute_id' => $contentclassattribute->id,
                    'contentobject_id' => $contentObject->id,
                    'jsondatas' => $this->buildValueContetObjectAttributes($contentClass->identifier, $post)
                ];
                foreach ($articleAttributes as $articleAttribute) {
                    ContentobjectAttributeModel::create($articleAttribute);
                }
        }

    }

    private function buildValueContetObjectAttributes($identifier, $post)
    {
        $value = [];
        switch ($identifier) {
            case 'intro':
                $value = ['value' => $post->resume];
                break;

            case 'content':
                $blockId = mt_rand(10000, 999999);
                $value = [$blockId => ['blockid' => $blockId, 'blocktype' => 'html', 'html' => html_entity_decode($post->description)]];
                break;

        }
        //attribut recette
        if ($post->posttypeid == self::RECETTE) {
            $postDetails = $this->dataProvider->getPostDetails($post->postid);
            foreach ($postDetails as $postDetail) {
                switch ($identifier) {
                    case 'details':
                        if ($postDetail->name == 'preparation') {
                            $value = ['content' => $postDetail->value];
                        }
                        break;

                    case 'duration':
                        if ($postDetail->name == 'duration') {
                            $time = trim(str_replace('minutes', '', $postDetail->value)) * 60;
                            $value = ['value' => $time];
                        }
                        break;

                    case 'nb_person':
                        if ($postDetail->name == 'persons') {
                            $value = ['value' => $postDetail->value];
                        }
                        break;

                    case 'ingredients':
                        if ($postDetail->name == 'ingredients') {
                            $blockId = mt_rand(10000, 999999);
                            $value = [$blockId => ['blockid' => $blockId, 'blocktype' => 'html', 'html' => $postDetail->value]];
                        }
                        break;
                }
            }
            unset($postDetails);
        }
        return $value;
    }

    public function generateDataForContentObject($postId, $contentclass_id, $parent_id, $postTitle, $postResume, $status)
    {
        return [
            "contentclass_id" => $contentclass_id,
            "parent_id" => $parent_id,
            "name" => $postTitle,
            "urltitle" => $postTitle,
            "metatitle" => $postTitle,
            "metadesc" => \Str::limit($postResume, 290),
            "metarobots" => 'index,follow',
            "tmpinfos" => null,
            "status" => $status,
            "remote_id" => 'post_' . $postId
        ];
    }

    public function generateDataForContentClass($contentclass_id, $postCategoryId, $parentId, $postCategoryTitle, $postCategoryName, $postCategoryDescription, $status)
    {
        return [
            "contentclass_id" => $contentclass_id,
            "parent_id" => $parentId,
            "name" => $postCategoryName,
            "urltitle" => $postCategoryName,
            "metatitle" => $postCategoryTitle,
            "metadesc" => $postCategoryDescription,
            "metarobots" => 'index,follow',
            "tmpinfos" => null,
            "status" => $status,
            "remote_id" => 'postcategory_' . $postCategoryId
        ];
    }

    public function attachTagToContentObject($data) //cette fonction à tester
    {
        foreach ($data as $tagPost) {
            $postName = $this->dataProvider->getPostNameById($tagPost->postid);
            if (isset($postName[0]->title)) {
                $contentObject = $this->dataProvider->getContentObjectByName($postName[0]->title);
            }
            if (!empty($contentObject)) {
                if (empty(TagContentObjectModel::where('contentobject_id', $contentObject['id']))) {
                    echo "+";
                    ContentobjectModel::where('name', $postName[0]->title)
                        ->first()
                        ->tags()
                        ->attach(explode(',', $tagPost->posttagids));
                } else {
                    echo "-";
                }
            }
        }
        unset($contentObject);
    }

    public function buildTag($data) //cette fonction à tester
    {
        $total = count($data);
        $cpt = 0;
        $limit = 200;

        while ($cpt < $total) {
            foreach ($data as $tag) {
                echo "!";
                $objTag = new TagModel();
                $objTag->name = $tag['name'];
                $objTag->save();
                unset($objTag);
                $cpt += $limit;
            }
        }
    }

    public function buildMenu($data, $parentId = null) //fonction à tester
    {
        foreach ($data as $row) {
            if (empty(MenuItemModel::where('title', $row['title'])->first())) {
                echo "!";
                $childs = [];
                if (isset($row['child'])) {
                    $childs = $row['child'];
                    unset($row['child']);
                }
                if ($parentId != null) {
                    $row['parent_id'] = $parentId;
                }

                if (!empty($this->dataProvider->getContentObjectByName($row['title']))) {
                    $row['url_externe'] = null;
                }

                $object = MenuItemModel::create($row);
                $object->save();
                if (count($childs)) {
                    $this->buildMenu($childs, $object->id);
                }
            } else {
                echo ".";
            }
        }
    }

    public function builSurHeaderMenu($nameItem)
    {
        return MenuItemModel::create([
            'title' => $nameItem,
            'parent_id' => 1,
            'url_externe' => 0,
            'is_blank' => 1
        ]);
    }

    public function buildBlock(ContentobjectModel $contentobjectModel)
    {
        $params = [
            'identifier' => 'xitems',
            'show_item_title' => 1,
            'show_item_desc' => 1,
            'show_item_img' => 1,
            'nb' => 10,
            'sourceauto' => [
                'name' => $contentobjectModel->name,
                'path' => $contentobjectModel->path_string
            ],
            'classes' => ContentclassModel::where("is_container", 0)->pluck('id')->toArray()
        ];

        (blocksModel::create([
            'name' => $contentobjectModel->name,
            'auto' => 1,
            'type' => '1itemsbyrows',
            'zone' => 'content',
            'params' => $params,
            'contentobject_id' => $contentobjectModel->id,
            'active' => 1,
            'values' => (new Xitems(['auto' => 1, 'params' => (array)$params], []))->getValues()
        ]))->save();

        unset($params);
    }

    public function buildRedirection($oldUrl, $newUrl)
    {
        $objRedirect = RedirectionModel::where('old_url', $oldUrl)->first();

        if (!$objRedirect) {
            (RedirectionModel::create([
                'old_url' => $oldUrl,
                'new_url' => $newUrl,
                'code' => 301,
            ]))->save();
        }
        unset($objRedirect);
    }
}
