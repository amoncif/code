<?php

namespace App\Console\Commands\Lib;

use Bayard\Admin\Contentmanager\Models\ContentobjectModel;
use Illuminate\Support\Facades\DB;

class ImportDataProvider
{
    const MENU_SUR_HEADER = [
        'https://www.familiscope.fr/sorties-famille/?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+sortie+famille&utm_content=pre+header' => 'Sortir en famille',
        'https://www.familiscope.fr/activites-enfants/?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+activites+enfants&utm_content=pre+header' => 'Activités enfants',
        'https://static.bayard.io/medias.notrefamille.com/lecture/lecture-pour-les-enfants.html?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+page+lecture&utm_content=pre+header' => 'Coin lecture',
        'https://e-bayard-jeunesse.com/offre-du-moment.html?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+btq+bj&utm_content=pre+header' => 'Nos magazines',
        'https://www.familiscope.fr/concours/?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+activites+jeu+concours&utm_content=pre+header' => 'Jeu-concours',
        'https://www.familiscope.fr/?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+site+nf&utm_content=pre+header' => 'Familiscope.fr',
        'https://www.notretemps.com/?utm_medium=ref&utm_source=notrefamille.com&utm_campaign=mea+site+nte&utm_content=pre+header' => 'Notretemps.com'
    ];

    public function getPostCategories($parentId)
    {
        return DB::connection('site')
            ->table('pws_post_category')
            ->select()
            ->where('parentid', $parentId ? $parentId : 0)
            ->get();
    }

    public function getMenuParents()
    {
        return DB::connection('site')
            ->select(
                'SELECT * FROM enfant.pws_menu where menuid in (select parentid from enfant.pws_menu)'
            );

    }

    public function getPostNameById($postid)
    {
        return DB::connection('site')
            ->select(
                "SELECT title FROM enfant.pws_post where postid = $postid"
            );
    }

    public function getTagsToImport()
    {
        return DB::connection('site')
            ->select(
                "SELECT posttagid,name FROM enfant.pws_post_tag"
            );
    }

    public function getMenuParentsAndChilds()
    {
        $menuTree = [];
        foreach ($this->getMenuParents() as $menuParent) {
            $menuTree [$menuParent->menuid] = [
                "title" => $menuParent->name,
                "parent_id" => $menuParent->parentid,
                "url_externe" => $menuParent->url,
                "contentobject_id" => $this->getContentObjectByName($menuParent->name) ? $this->getContentObjectByName($menuParent->name)['id'] : null,
                "child" => $this->getMenuFromDataBaseByParentId($menuParent->menuid)
            ];
        }
        return $menuTree;
    }

    public function getMenuFromDataBaseByParentId($parentid)
    {
        $menuChilds = DB::connection('site')
            ->table('pws_menu')
            ->where('parentid', $parentid)
            ->get()
            ->toArray();
        $childs = [];
        foreach ($menuChilds as $child) {
            $childs [] = [
                "title" => $child->name,
                "parent_id" => $child->parentid,
                "url_externe" => $child->url,
                "contentobject_id" => $this->getContentObjectByName($child->name) ? $this->getContentObjectByName($child->name)['id'] : null,
                "child" => $this->getMenuFromDataBaseByParentId($child->menuid)
            ];
        }
        return $childs;
    }

    public function getPostTagIds()
    {
        return DB::connection('site')
            ->select(
                "SELECT postid, posttagids FROM enfant.pws_post where posttagids <> '' "
            );
    }

    public function getPostsCount($postTypeIds)
    {
        return DB::connection('site')
            ->table('pws_post')
            ->where('postcategoryid', '>', 0)
            ->whereIn('posttypeid', $postTypeIds)
            ->count();
    }

    public function getPostsToImport($postTypeIds, $cpt, $limit)
    {
        return DB::connection('site')
            ->table('pws_post')
            ->where('postcategoryid', '>', 0)
            ->whereIn('posttypeid', $postTypeIds)
            ->offset($cpt)
            ->limit($limit)
            ->get();
    }

    public function getPostDetails(int $postId)
    {
        return DB::connection('site')
            ->table('pws_post_detail')
            ->where('postid', $postId)
            ->get();

    }

    public function getContentObjectByName($name)
    {
        return ContentobjectModel::where('name', $name)->first() ? ContentobjectModel::where('name', $name)->first()->toArray() : [];
    }

}
